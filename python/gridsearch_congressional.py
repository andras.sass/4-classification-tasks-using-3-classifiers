import pickle as pkl

import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
import os

from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import RobustScaler
from sklearn.svm import SVC

from sklearn.model_selection import train_test_split, cross_validate, GridSearchCV

from congressional_voting import load_preprocessed_data

X, y = load_preprocessed_data()

PREFIX = 'congresional_'
FOLDER = 'gridsearch_cv'

# Feel free to edit the search spaces as you see fit
search_space_knn = {
    'n_neighbors': list(range(1, 42)),
    'weights': ['uniform', 'distance'],
    'metric': ['euclidean', 'manhattan', 'chebyshev', 'minkowski'],
    'algorithm': ['auto', 'ball_tree', 'kd_tree', 'brute'],
}
search_space_rf = {
    'n_estimators': [1]+list(range(25, 250, 25)),
    'max_depth': [1]+list(range(25, 250, 25)),
    'criterion': ['gini', 'entropy'],

}
search_space_svm = {
    'kernel': ['linear', 'poly', 'rbf', 'sigmoid'],
    # 'degree':list(range(1,15)),
    'C': list(np.arange(0.1, 1, 0.1))+list(range(2, 10, 1)),
}


# Change verbose to 1 to see less output
print('Searching KNN')
gscv_knn = GridSearchCV(KNeighborsClassifier(),
                        search_space_knn, cv=5, verbose=2, n_jobs=-1)
rv_knn = gscv_knn.fit(X, y)
pkl.dump(rv_knn, open(os.path.join(FOLDER, f'{PREFIX}rv_knn.p'), 'wb'))

print('Searching RF')
gscv_rf = GridSearchCV(RandomForestClassifier(),
                       search_space_rf, cv=5, verbose=2, n_jobs=-1)
rv_rf = gscv_rf.fit(X, y)
pkl.dump(rv_rf, open(os.path.join(f'{PREFIX}rv_rf.p'), 'wb'))

print('Searching SVM')
gscv_svm = GridSearchCV(SVC(), search_space_svm, cv=5, verbose=2, n_jobs=-1)
rv_svm = gscv_svm.fit(X, y)
pkl.dump(rv_svm, open(os.path.join(f'{PREFIX}rv_svm.p'), 'wb'))


best_knn_estimator = rv_knn.best_estimator_
best_rf_estimator = rv_rf.best_estimator_
best_svm_estimator = rv_svm.best_estimator_

print("Best KNN params:")
print(best_knn_estimator.get_params())
print("Best RF params:")
print(best_rf_estimator.get_params())
print("Best SVM params:")
print(best_svm_estimator.get_params())
