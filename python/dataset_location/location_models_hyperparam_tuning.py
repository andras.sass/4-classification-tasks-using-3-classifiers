import pandas as pd
import numpy as np
import joblib
from pandas.api.types import CategoricalDtype
from sklearn.model_selection import GridSearchCV
from sklearn.decomposition import PCA
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.pipeline import Pipeline

np.random.seed(42)

df = pd.read_csv(
    "../data/kaggle_location_dataset/Location446-30cls-5k.lrn.csv")
geosocial_type = CategoricalDtype(
    categories=[f"{i}" for i in range(1, 31)], ordered=True)
df['class'] = df['class'].apply(str).astype(geosocial_type)
df = df.drop("ID", axis=1)
df = df.sample(frac=1)

X = df.iloc[:, 1:]
y = df["class"]
print("ETL done")

# # ------------   KNN   ------------
pca = PCA()
knn = KNeighborsClassifier(metric='minkowski')
pipe = Pipeline(steps=[("pca", pca), ("knn", knn)])
param_grid = {
    "pca__n_components": range(10, 51, 2),
    "knn__n_neighbors": range(30, 101, 2),
    "knn__p": [1, 2],
    "knn__weights": ["uniform", "distance"],
}
knn_search = GridSearchCV(pipe, param_grid, n_jobs=6, cv=5, verbose=2)
knn_search.fit(X, y)
joblib.dump(knn_search, 'KNN_gridsearch.pkl')
print("KNN done")

# ------------   RF   ------------
pca = PCA()
rf = RandomForestClassifier()
pipe = Pipeline(steps=[("pca", pca), ("rf", rf)])
param_grid = {
    "pca__n_components": range(10, 51, 2),
    "rf__n_estimators": range(1000, 3501, 500),
    "rf__max_depth": [25, 50, None],
    "rf__criterion": ["gini", "entropy"],
}
rf_search = GridSearchCV(pipe, param_grid, n_jobs=6, cv=5, verbose=2)
rf_search.fit(X, y)
joblib.dump(rf_search, 'RF_gridsearch.pkl')
print("RF done")

# ------------   SVM   ------------
pca = PCA()
svm = SVC()
pipe = Pipeline(steps=[("pca", pca), ("svm", svm)])
param_grid = {
    "pca__n_components": range(10, 51, 2),
    "svm__kernel": ["linear", "rbf", "sigmoid", "poly"],
    "svm__C": np.linspace(0.1, 2, 20)
}
svm_search = GridSearchCV(pipe, param_grid, n_jobs=9, cv=5, verbose=2)
svm_search.fit(X, y)
joblib.dump(svm_search, 'SVM_gridsearch.pkl')
print("SVM done")


best_knn_estimator = knn_search.best_estimator_
best_rf_estimator = rf_search.best_estimator_
best_svm_estimator = svm_search.best_estimator_

print("Best KNN params:")
print(best_knn_estimator.get_params())
print("Best RF params:")
print(best_rf_estimator.get_params())
print("Best SVM params:")
print(best_svm_estimator.get_params())
