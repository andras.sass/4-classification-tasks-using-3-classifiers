import pandas as pd
import numpy as np
import joblib
from scipy.io import arff
from sklearn.preprocessing import RobustScaler, PowerTransformer
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC

np.random.seed(42)


def load_preprocessed_bankruptcy(scale=True, splitXY=True):
    # def assign_variable_names(unnamed_data):
    arff_data = arff.loadarff('../data/polish_companies_bankruptcy/5year.arff')
    ds = pd.DataFrame(arff_data[0])
    variable_names = ["net profit / total assets",
                      "total liabilities / total assets",
                      "working capital / total assets",
                      "current assets / short - term liabilities",
                      "[(cash + short - term securities + receivables - short-term liabilities) / (operating expenses - depreciation)] * 365",
                      "retained earnings / total assets",
                      "EBIT / total assets",
                      "book value of equity / total liabilities",
                      "sales / total assets",
                      "equity / total assets",
                      "(gross profit + extraordinary items + financial expenses) / total assets",
                      "gross profit / short - term liabilities",
                      "(gross profit + depreciation) / sales",
                      "(gross profit + interest) / total assets",
                      "(total liabilities * 365) / (gross profit + depreciation)",
                      "(gross profit + depreciation) / total liabilities",
                      "total assets / total liabilities",
                      "gross profit / total assets",
                      "gross profit / sales",
                      "(inventory * 365) / sales",
                      "sales(n) / sales(n - 1)",
                      "profit on operating activities / total assets",
                      "net profit / sales",
                      "gross profit( in 3 years) / total assets",
                      "(equity - share capital) / total assets",
                      "(net profit + depreciation) / total liabilities",
                      "profit on operating activities / financial expenses",
                      "working capital / fixed assets",
                      "logarithm of total assets",
                      "(total liabilities - cash) / sales",
                      "(gross profit + interest) / sales",
                      "(current liabilities * 365) / cost of products sold",
                      "operating expenses / short - term liabilities",
                      "operating expenses / total liabilities",
                      "profit on sales / total assets",
                      "total sales / total assets",
                      "(current assets - inventories) / long - term liabilities",
                      "constant capital / total assets",
                      "profit on sales / sales",
                      "(current assets - inventory - receivables) / short - term liabilities",
                      "total liabilities / ((profit on operating activities + depreciation) * (12 / 365))",
                      "profit on operating activities / sales",
                      "rotation receivables + inventory turnover in days",
                      "(receivables * 365) / sales",
                      "net profit / inventory",
                      "(current assets - inventory) / short - term liabilities",
                      "(inventory * 365) / cost of products sold",
                      "EBITDA(profit on operating activities - depreciation) / total assets",
                      "EBITDA(profit on operating activities - depreciation) / sales",
                      "current assets / total liabilities",
                      "short - term liabilities / total assets",
                      "(short - term liabilities * 365) / cost of products sold)",
                      "equity / fixed assets",
                      "constant capital / fixed assets",
                      "working capital",
                      "(sales - cost of products sold) / sales",
                      "(current assets - inventory - short - term liabilities) / (sales - gross profit - depreciation)",
                      "total costs / total sales",
                      "long - term liabilities / equity",
                      "sales / inventory",
                      "sales / receivables",
                      "(short - term liabilities * 365) / sales",
                      "sales / short - term liabilities",
                      "sales / fixed assets",
                      "bankruptcy"]

    ds = ds.set_axis(variable_names, axis=1, inplace=False)

    # binary cat to int
    ds['bankruptcy'] = ds['bankruptcy'].map({b'0': 0, b'1': 1})

    # Removing variables with too many NaNs
    ds = ds.drop(['(current assets - inventories) / long - term liabilities',
                 'profit on operating activities / financial expenses', 'sales / inventory', 'net profit / inventory'], axis=1)

    # Dropping the remaining samples with NaNs
    ds = ds.dropna()

    # Dropping duplicatel lines
    ds = ds.drop_duplicates()

    y = ds['bankruptcy']
    X = ds.drop(['bankruptcy'], axis=1)

    if scale:
        scaler_1 = PowerTransformer()
        scaler_2 = RobustScaler()
        X = scaler_1.fit_transform(X)
        X = scaler_2.fit_transform(X)

    if splitXY:
        return X, y
    else:
        return ds


X, y = load_preprocessed_bankruptcy(scale=True, splitXY=True)
print("ETL done")

# ------------   KNN   ------------
knn = KNeighborsClassifier(metric='minkowski')
param_grid = {
    "n_neighbors": range(2, 36, 2),
    "p": [1, 2],
    "weights": ["uniform", "distance"],
}
knn_search = GridSearchCV(knn, param_grid, n_jobs=-1, cv=5)
knn_search.fit(X, y)
joblib.dump(knn_search, 'bankruptcy_KNN_gridsearch.pkl')
print("KNN done")

# ------------   RF   ------------
rf = RandomForestClassifier()
param_grid = {
    "n_estimators": range(100, 401, 20),
    "max_depth": [2, 3, 5, 10, 25, None],
    "criterion": ["gini", "entropy"],
}
rf_search = GridSearchCV(rf, param_grid, n_jobs=-1, cv=5)
rf_search.fit(X, y)
joblib.dump(rf_search, 'bankruptcy_RF_gridsearch.pkl')
print("RF done")

# ------------   SVM   ------------
svm = SVC()
param_grid = {
    "kernel": ["rbf", "sigmoid"],
    "c": np.linspace(0.1, 2, 20)
}
svm_search = GridSearchCV(svm, param_grid, n_jobs=-1, cv=5)
svm_search.fit(X, y)
joblib.dump(svm_search, 'bankruptcy_SVM_gridsearch.pkl')
print("SVM done")


best_knn_estimator = knn_search.best_estimator_
best_rf_estimator = rf_search.best_estimator_
best_svm_estimator = svm_search.best_estimator_

print("Best KNN params:")
print(best_knn_estimator.get_params())
print("Best RF params:")
print(best_rf_estimator.get_params())
print("Best SVM params:")
print(best_svm_estimator.get_params())
