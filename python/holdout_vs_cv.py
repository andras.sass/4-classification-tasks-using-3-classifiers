import numpy as np
import copy
import time

from numpy.core._multiarray_umath import ndarray
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score, f1_score

"""
import compare_holdout_vs_cv from holdout_vs_cv
1. Preprocess your data and define X and y

knn = KNeighborsClassifier(n_neighbors=40, p=1, metric='minkowski')
rf = RandomForestClassifier(n_estimators=200)
svm = SVC(kernel="linear", random_state=42)

best_models = {"knn": knn, "r_forest": rf, "svm": svm}

ho_vs_cv_scores = compare_holdout_vs_cv(models=best_models,
                                        X_preprocessed=X,
                                        y_preprocessed=y,
                                        k_folds=5,
                                        n_seeds=5)
"""


def compare_holdout_vs_cv(models, X_preprocessed, y_preprocessed, k_folds=5, n_seeds=50):
    """Compute the accuracy and F1 score of the passed models on the passed data "n_seeds" times and average them.

        Args:
            models (dict):                  Dictionary with keys "knn", "r_forest" and "svm". Each entry of the dict should be the
                                            respective (untrained) model with the optimal hyperparameters
            X_preprocessed ([DataFrame]):   The data on which to perform CV and hold-out evaluation
            y_preprocessed ([DataFrame]):   The data on which to perform CV and hold-out evaluation
            k_folds (int, optional):        Number of folds for CV. Affects the train-test split raio which will be calculated
                                            as 1/k_folds
            n_seeds (int, optional):        Over how many random states should be averaged

        Returns:
            [Dict]: Contains "knn", "r_forest", "svm" which are dicts and contain the corresponding calculated metrics
        """

    score = {"ho_mean": None, "ho_std": None, "cv_mean": None, "cv_std": None, "ho_scores": [], "cv_scores": []}
    model_scores = {"accuracy": copy.deepcopy(score), "f1": copy.deepcopy(score), "duration": copy.deepcopy(score)}
    scores = {"knn": copy.deepcopy(model_scores),
              "r_forest": copy.deepcopy(model_scores),
              "svm": copy.deepcopy(model_scores)}

    for rnd_seed in range(1, n_seeds + 1, 1):
        X, y = shuffle(X_preprocessed, y_preprocessed, random_state=rnd_seed)

        for model_name in models:
            # Calculate hold-out scores
            model = models[model_name]
            # start time
            ho_start = time.time()
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=1 / k_folds, shuffle=False)
            model.fit(X_train, y_train)
            y_pred = model.predict(X_test)

            ho_accuracy = accuracy_score(y_test, y_pred)
            ho_f1 = f1_score(y_test, y_pred, average='macro')  # Use macro averaging
            # end time
            ho_end = time.time()
            
            scores[model_name]["accuracy"]["ho_scores"].append(ho_accuracy)
            scores[model_name]["f1"]["ho_scores"].append(ho_f1)
            scores[model_name]["duration"]["ho_scores"].append(ho_end - ho_start)

            # Calculate CV scores
            model = models[model_name]
            # start time
            cv_start = time.time()
            cv_accuracies = cross_val_score(model, X, y, scoring="accuracy", n_jobs=-1, cv=k_folds)
            cv_accuracy = np.mean(cv_accuracies)
            cv_f1s = cross_val_score(model, X, y, scoring="f1_macro", n_jobs=-1, cv=k_folds)
            cv_f1 = np.mean(cv_f1s)
            # end time
            cv_end = time.time()
            scores[model_name]["accuracy"]["cv_scores"].append(cv_accuracy)
            scores[model_name]["f1"]["cv_scores"].append(cv_f1)
            scores[model_name]["duration"]["cv_scores"].append(cv_end - cv_start)

        print(f"\r Scores calculated for random seed {rnd_seed} / {n_seeds}", end="")

    for model_name in models:
        print(f"\n\n{model_name}")
        for score_name in model_scores:
            print(f"   {score_name}:")
            ho_mean = np.mean(scores[model_name][score_name]["ho_scores"])
            ho_std = np.std(scores[model_name][score_name]["ho_scores"])
            cv_mean = np.mean(scores[model_name][score_name]["cv_scores"])
            cv_std = np.std(scores[model_name][score_name]["cv_scores"])

            scores[model_name][score_name]["ho_mean"] = ho_mean
            scores[model_name][score_name]["ho_std"] = ho_std
            scores[model_name][score_name]["cv_mean"] = cv_mean
            scores[model_name][score_name]["cv_std"] = cv_std

            print(f"      Average Hold-out {score_name}: {round(ho_mean, 5)} +- {round(ho_std, 5)}")
            print(f"      Average    CV    {score_name}: {round(cv_mean, 5)} +- {round(cv_std, 5)}")

    return scores
