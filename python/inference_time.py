from time import perf_counter
import numpy as np

"""
Usage example

import compute_inference_time from inference_time
model = XGBRFClassifier()
print(compute_inference_time(model.predict, test_X))
"""


def compute_inference_time(pred_fun, test_X, n=1000, len_x=100):
    """Compute the prediction time `n` times on with `len_x` samples

    Args:
        pred_fun ([type]): the predictor function
        test_X ([type]): Test dataset
        n (int, optional): Number of repetitions. Defaults to 500.
        len_X (int, optional): Length of the subset taken from the testset. Defaults to 100.

    Returns:
        [tuple]: at [0] the mean runtime; at [1] the std. deviation
    """
    test_X = test_X[:len_x]
    runtimes = np.empty(n)
    for i in range(n):
        with catchtime() as t:
            pred_fun(test_X)
        runtimes[i] = t.time
    return (np.mean(runtimes), np.std(runtimes))


class catchtime:
    def __enter__(self):
        self.time = perf_counter()
        return self

    def __exit__(self, type, value, traceback):
        self.time = perf_counter() - self.time
