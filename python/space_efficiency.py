import os
import tempfile
import pickle
from joblib import dump


def serialized_disk_space(model):
    # tmpdir = tempfile.gettempdir()
    # tmpfile = os.path.join(tmpdir, 'test.joblib')
    # dump(model, tmpfile)

    # The length of the pickle in memory will be used over the size of
    # built-in scikit learn object serializer because they only differ in bytes
    s = pickle.dumps(model)
    return len(s)
