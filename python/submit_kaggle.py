from pandas.api.types import CategoricalDtype
from sklearn.decomposition import PCA
from sklearn.pipeline import make_pipeline
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt

from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.metrics import classification_report
import joblib
from sklearn.pipeline import Pipeline

import os


from preprocessing import load_preprocessed_hawks, load_preprocessed_congressional, load_preprocessed_bankruptcy, load_preprocessed_location

from inference_time import compute_inference_time
from space_efficiency import serialized_disk_space

USE_OPTIMAL_PARAMS = True
ALGORITHMS = ['svm', 'knn', 'rf']
# target_ds = 'congresional'
# target_ds = 'hawks'
# target_ds = 'bankruptcy'
target_ds = 'location'


def load_best_params(dataset, folder='gridsearch_cv'):
    """Load best params from joblib file"""
    params = {}
    for algorithm in ALGORITHMS:
        with open(os.path.join(folder, f'{dataset}_rv_{algorithm}.pkl'), 'rb') as pkl:
            model = joblib.load(pkl)
            params[algorithm] = model.best_estimator_.get_params()
    return params


def clean_args(kwargdict, prefix):
    cleaned = {}
    for key in kwargdict.keys():
        if prefix in key:
            cleaned[key.replace(prefix, '')] = kwargdict[key]
    return cleaned


def load_preprocessed_location_kaggle():
    df = pd.read_csv(
        "data/kaggle_location_dataset/Location446-30cls-5k.tes.csv")
    # df = df.drop("ID", axis=1)
    # df = df.sample(frac=1)
    return df


def prepare_kaggle_submission(model, df_test):
    df_test = load_preprocessed_location_kaggle()
    ypred = model.predict(df_test.drop("ID", axis=1))
    y_submit = pd.DataFrame(ypred)
    # y_submit.replace([1, 0], ['republican', 'democrat'], inplace=True)
    y_submit['ID'] = df_test['ID']
    y_submit.rename(columns={0: 'class'}, inplace=True)
    y_submit.reindex(columns=['ID', 'class'])
    y_submit.set_index('ID')
    y_submit.to_csv('location_kaggle.csv', columns=[
                    'ID', 'class'], index=False)


np.random.seed(42)

if target_ds == 'location':
    X, y = load_preprocessed_location()

print(f"Testing with optimal parameters for {target_ds}")

models = {}
params = {}

if USE_OPTIMAL_PARAMS:
    params = load_best_params(target_ds)

# Random Forrest
# if target_ds == 'location':
#     pca_args = clean_args(params['rf'], 'pca__')
#     rf_args = clean_args(params['rf'], 'rf__')

#     pca = PCA(**pca_args)
#     knn = RandomForestClassifier(**rf_args)

#     rf_model = make_pipeline(pca, knn)
# else:
#     rf_args = params['rf']
#     rf_model = RandomForestClassifier(**rf_args)

# rf_model.fit(X, y)
# models['rf'] = rf_model


# Train SVM
if target_ds == 'location':
    pca_args = clean_args(params['svm'], 'pca__')
    svm_args = clean_args(params['svm'], 'svm__')

    pca = PCA(**pca_args)
    svm = SVC(**svm_args)

    svm_model = make_pipeline(pca, svm)
else:
    svm_args = params['svm']
    svm_model = SVC(**svm_args)

svm_model.fit(X, y)
# models['svm'] = svm_model


# Train k-NN
# if target_ds == 'location':
#     pca_args = clean_args(params['knn'], 'pca__')
#     knn_args = clean_args(params['knn'], 'knn__')

#     pca = PCA(**pca_args)
#     knn = KNeighborsClassifier(**knn_args)

#     knn_model = make_pipeline(pca, knn)
# else:
#     knn_args = params['knn']
#     knn_model = KNeighborsClassifier(**knn_args)

# knn_model.fit(X, y)
# models['knn'] = knn_model

prepare_kaggle_submission(svm_model, None)
