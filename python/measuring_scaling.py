
import numpy as np
import sklearn

from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score

from matplotlib import pyplot as plt

from preprocessing import load_preprocessed_bankruptcy, load_preprocessed_hawks


np.random.seed(42)
nseeds = 10

plt.figure()

for n_ds, dataset in enumerate(['hawks','bankruptcy']):

    range_hp = {
        'knn':range(1,30),
        'rf':[1]+list(range(25, 250, 25)),
        'svm':list(np.arange(0.1,1,0.1))+[2,3,4,5]
    }
    accs_sc = {
     'knn':np.zeros([nseeds, len(range_hp['knn'])]),
     'rf': np.zeros([nseeds, len(range_hp['rf'])]),
     'svm': np.zeros([nseeds, len(range_hp['svm'])])
    }

    accs = {
        'knn': np.zeros([nseeds, len(range_hp['knn'])]),
        'rf': np.zeros([nseeds, len(range_hp['rf'])]),
        'svm': np.zeros([nseeds, len(range_hp['svm'])])
    }

    hp_name = {
        'knn':'K',
        'rf':'n_estimators',
        'svm':'C'
    }

    # dirty but does the job:
    for n_clf, classifier_code in enumerate(['knn','rf','svm']):

        for i, rs in enumerate(range(0,nseeds)):

            if dataset == 'hawks':
                X, y = load_preprocessed_hawks(scale=False, splitXY=True)
            elif dataset == 'bankruptcy':
                X, y = load_preprocessed_bankruptcy(scale=False, splitXY=True)
            X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(X, y, test_size=0.2, random_state = rs)

            if dataset == 'hawks':
                X, y = load_preprocessed_hawks(scale=True, splitXY=True)
            elif dataset == 'bankruptcy':
                X, y = load_preprocessed_bankruptcy(scale=True, splitXY=True)
            Xsc_train, Xsc_test, ysc_train, ysc_test = sklearn.model_selection.train_test_split(X, y, test_size=0.2, random_state = rs)

            for j, hp in enumerate(range_hp[classifier_code]):
                if classifier_code == 'knn':
                    clf = KNeighborsClassifier(n_neighbors=hp)
                elif classifier_code == 'rf':
                    clf = RandomForestClassifier(n_estimators=hp)
                elif classifier_code == 'svm':
                    clf = SVC(C=hp)
                clf.fit(Xsc_train,ysc_train)
                accs_sc[classifier_code][i][j] = accuracy_score(ysc_test, clf.predict(Xsc_test))

                clf.fit(X_train, y_train)
                accs[classifier_code][i][j] = accuracy_score(y_test, clf.predict(X_test))

        plt.subplot(3,2,2*n_clf+n_ds+1)
        plt.plot(range_hp[classifier_code],np.mean(accs[classifier_code], axis=0), label='Nonscaled')
        plt.plot(range_hp[classifier_code],np.mean(accs_sc[classifier_code], axis=0), label='Scaled')
        plt.legend()
        plt.title(f'Impact of Scaling - {classifier_code} - {dataset} dataset')
        plt.xlabel(hp_name[classifier_code])
        plt.ylabel('Accuracy')

plt.tight_layout()
plt.show()