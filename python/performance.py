from sklearn.decomposition import PCA
from sklearn.pipeline import make_pipeline
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt

from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.metrics import classification_report
import joblib
from sklearn.pipeline import Pipeline

import os


from preprocessing import load_preprocessed_hawks, load_preprocessed_congressional, load_preprocessed_bankruptcy, load_preprocessed_location

from inference_time import compute_inference_time
from space_efficiency import serialized_disk_space

USE_OPTIMAL_PARAMS = True
ALGORITHMS = ['svm', 'knn', 'rf']
# target_ds = 'congresional'
# target_ds = 'hawks'
# target_ds = 'bankruptcy'
target_ds = 'location'


def load_best_params(dataset, folder='gridsearch_cv'):
    """Load best params from joblib file"""
    params = {}
    for algorithm in ALGORITHMS:
        with open(os.path.join(folder, f'{dataset}_rv_{algorithm}.pkl'), 'rb') as pkl:
            model = joblib.load(pkl)
            params[algorithm] = model.best_estimator_.get_params()
    return params


def clean_args(kwargdict, prefix):
    cleaned = {}
    for key in kwargdict.keys():
        if prefix in key:
            cleaned[key.replace(prefix, '')] = kwargdict[key]
    return cleaned


np.random.seed(42)

if target_ds == 'congresional':
    X, y = load_preprocessed_congressional()
elif target_ds == 'hawks':
    X, y = load_preprocessed_hawks()
elif target_ds == 'bankruptcy':
    X, y = load_preprocessed_bankruptcy()
elif target_ds == 'location':
    X, y = load_preprocessed_location()

X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.20, random_state=42)

print(f"Testing with optimal parameters for {target_ds}")

models = {}
params = {}

if USE_OPTIMAL_PARAMS:
    params = load_best_params(target_ds)

# Random Forrest
if target_ds == 'location':
    pca_args = clean_args(params['rf'], 'pca__')
    rf_args = clean_args(params['rf'], 'rf__')

    pca = PCA(**pca_args)
    knn = RandomForestClassifier(**rf_args)

    rf_model = make_pipeline(pca, knn)
else:
    rf_args = params['rf']
    rf_model = RandomForestClassifier(**rf_args)

rf_model.fit(X_train, y_train)
models['rf'] = rf_model


# Train SVM
if target_ds == 'location':
    pca_args = clean_args(params['svm'], 'pca__')
    svm_args = clean_args(params['svm'], 'svm__')

    pca = PCA(**pca_args)
    svm = SVC(**svm_args)

    svm_model = make_pipeline(pca, svm)
else:
    svm_args = params['svm']
    svm_model = SVC(**svm_args)

svm_model.fit(X_train, y_train)
models['svm'] = svm_model


# Train k-NN
if target_ds == 'location':
    pca_args = clean_args(params['knn'], 'pca__')
    knn_args = clean_args(params['knn'], 'knn__')

    pca = PCA(**pca_args)
    knn = KNeighborsClassifier(**knn_args)

    knn_model = make_pipeline(pca, knn)
else:
    knn_args = params['knn']
    knn_model = KNeighborsClassifier(**knn_args)

knn_model.fit(X_train, y_train)
models['knn'] = knn_model


for algorithm in ALGORITHMS:
    yhat = models[algorithm].predict(X_test)
    res = classification_report(y_test, yhat, output_dict=True)
    inf_time = compute_inference_time(models[algorithm].predict, X_test)
    disk_space = serialized_disk_space(models[algorithm])
    print(algorithm)
    print(f"accuracy \t {res['accuracy']:2f}")
    print(f"f1\t\t {res['macro avg']['f1-score']:2f}")
    print(f'pred. time\t {inf_time[0]:.2e} +- {inf_time[1]:.2e}')
    print(f'memory\t\t {disk_space:.2e}')
    print()

print("Optimal parameters for RF:")
print(f"N Trees: \t {rf_args['n_estimators']}")
print(f"Max Depth: \t  {rf_args['max_depth']}")
print(f"Criterion: \t {rf_args['criterion']}")

print("Optimal parameters for KNN:")
print(f"K: \t\t {knn_args['n_neighbors']}")
print(f"Weights: \t\t  {knn_args['weights']}")
print(f"Distance metric: \t {knn_args['metric']}")

print("Optimal parameters for SVM:")
print(f"Kernel: \t {svm_args['kernel']}")
print(f"C: \t {svm_args['C']}")
