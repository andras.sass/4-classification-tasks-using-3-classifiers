from inference_time import compute_inference_time
from space_efficiency import serialized_disk_space
from sklearn.model_selection import train_test_split
from sklearn.impute import KNNImputer
import numpy as np
import pandas as pd

from xgboost import XGBRFClassifier

from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report
from sklearn.model_selection import GridSearchCV


USE_OPTIMAL_PARAMS = False


def load_preprocessed_data(splitXY=True):
    df = prepare_and_load_data(
        'data/kaggle_congressional_voting/CongressionalVotingID.shuf.lrn.csv')

    X = df.drop(columns=['ID', 'class'])
    y = df['class']

    imputer = KNNImputer(n_neighbors=1)
    imputer.fit(X)
    X = imputer.transform(X)
    return X, y


def prepare_and_load_data(dataset):
    df = pd.read_csv(dataset)

    df = df.replace(['y', 'n', 'unknown', 'republican',
                     'democrat'], [0, 1, np.nan, 1, 0])
    return df


def prepare_kaggle_submission(ypred, df_test):
    y_submit = pd.DataFrame(ypred)
    y_submit.replace([1, 0], ['republican', 'democrat'], inplace=True)
    y_submit['ID'] = df_test['ID']
    y_submit.rename(columns={0: 'class'}, inplace=True)
    y_submit.reindex(columns=['ID', 'class'])
    y_submit.set_index('ID')
    y_submit.to_csv('test.csv', columns=['ID', 'class'], index=False)


# Load data
df = prepare_and_load_data(
    'data/kaggle_congressional_voting/CongressionalVotingID.shuf.lrn.csv')
X = df.drop(columns=['ID', 'class'])
y = df['class']


if __name__ == '__main__':
    # Impute missing data
    imputer = KNNImputer(n_neighbors=1)

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.20, random_state=42)

    imputer.fit(X_train)

    X_train = imputer.transform(X_train)
    X_test = imputer.transform(X_test)

    rf_gs_params = {}
    if USE_OPTIMAL_PARAMS:
        rf_gs_params = {'bootstrap': True, 'ccp_alpha': 0.0, 'class_weight': None, 'criterion': 'gini', 'max_depth': 150, 'max_features': 'auto', 'max_leaf_nodes': None, 'max_samples': None, 'min_impurity_decrease': 0.0,
                        'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 125, 'n_jobs': None, 'oob_score': False, 'random_state': None, 'verbose': 0, 'warm_start': False}
    rf_model = RandomForestClassifier(**rf_gs_params)
    rf_model.fit(X_train, y_train)

    # Train SVM
    svm_gs_params = {}
    if USE_OPTIMAL_PARAMS:
        svm_gs_params = {'C': 0.6, 'break_ties': False, 'cache_size': 200, 'class_weight': None, 'coef0': 0.0, 'decision_function_shape': 'ovr', 'degree': 3,
                         'gamma': 'scale', 'kernel': 'rbf', 'max_iter': -1, 'probability': False, 'random_state': None, 'shrinking': True, 'tol': 0.001, 'verbose': False}
    svm_model = svm.SVC(**svm_gs_params)
    svm_model.fit(X_train, y_train)

    # Train k-NN
    knn_gs_params = {}
    if USE_OPTIMAL_PARAMS:
        knn_gs_params = {'algorithm': 'auto', 'leaf_size': 30, 'metric': 'manhattan',
                         'metric_params': None, 'n_jobs': None, 'n_neighbors': 1, 'p': 2, 'weights': 'uniform'}
    knn_model = KNeighborsClassifier(**knn_gs_params)
    knn_model.fit(X_train, y_train)

    # Evaluation
    df_kaggle_test = prepare_and_load_data(
        'data/kaggle_congressional_voting/CongressionalVotingID.shuf.tes.csv')

    X_kaggle = df_kaggle_test.drop(columns=['ID'])
    X_kaggle = imputer.transform(X_kaggle)

    # Evaluate Random Forest Model
    yhat = rf_model.predict(X_test)
    print("Random Forest Report")
    print(classification_report(y_test, yhat))

    inference_time = compute_inference_time(rf_model.predict, X_test)
    print(
        f"Inference time mean: {inference_time[0]:e} \nStd. deviation: {inference_time[0]}")
    print(f"Size of model in {serialized_disk_space(rf_model)} byte")

    rf_yhat = rf_model.predict(X_kaggle)
    # prepare_kaggle_submission(rf_yhat, df_kaggle_test)

    # Evaluate SVM
    yhat = svm_model.predict(X_test)
    print("SVM Report")
    print(classification_report(y_test, yhat))
    inference_time = compute_inference_time(svm_model.predict, X_test)
    print(
        f"Inference time mean: {inference_time[0]:e} \nStd. deviation: {inference_time[0]}")
    print(f"Size of model in {serialized_disk_space(svm_model)} byte")

    svm_yhat = svm_model.predict(X_kaggle)
    prepare_kaggle_submission(svm_yhat, df_kaggle_test)

    # Evaluate k-NN
    yhat = knn_model.predict(X_test)
    print("k-NN Report")
    print(classification_report(y_test, yhat))
    inference_time = compute_inference_time(knn_model.predict, X_test)
    print(
        f"Inference time mean: {inference_time[0]:e} \nStd. deviation: {inference_time[0]}")
    print(f"Size of model in {serialized_disk_space(knn_model)} byte")

    knn_yhat = svm_model.predict(X_kaggle)
    # prepare_kaggle_submission(knn_yhat, df_kaggle_test)
