import numpy as np
import pandas as pd
from pandas.api.types import CategoricalDtype

from sklearn.impute import KNNImputer
from sklearn.preprocessing import RobustScaler, PowerTransformer
from scipy.io import arff


def load_preprocessed_hawks(scale=True, splitXY=True):
    ds = pd.read_csv('data/hawks/data.csv')

    # removing metadata features
    ds = ds.drop(['Month', 'Day', 'Year', 'CaptureTime',
                 'ReleaseTime', 'BandNumber'], axis=1)

    # removing features that are mostly NaN
    ds = ds.drop(['Sex', 'Tarsus', 'WingPitFat', 'KeelFat',
                 'Crop', 'StandardTail'], axis=1)

    # removing rows that still have nans:
    ds = ds.dropna()

    # removing hallux outliers
    ds = ds.drop(ds[ds['Hallux'] > 150].index)

    # binary cat to int
    ds['Age'] = ds['Age'].map({'A': 1, 'I': 0})

    y = ds['Species']
    X = ds.drop(['Species'], axis=1)

    if scale:
        scaler = RobustScaler()
        X = scaler.fit_transform(X)

    if splitXY:
        return X, y
    else:
        return ds


def load_preprocessed_bankruptcy(scale=True, splitXY=True):
    arff_data = arff.loadarff('data/polish_companies_bankruptcy/5year.arff')
    ds = pd.DataFrame(arff_data[0])
    variable_names = ["net profit / total assets",
                      "total liabilities / total assets",
                      "working capital / total assets",
                      "current assets / short - term liabilities",
                      "[(cash + short - term securities + receivables - short-term liabilities) / (operating expenses - depreciation)] * 365",
                      "retained earnings / total assets",
                      "EBIT / total assets",
                      "book value of equity / total liabilities",
                      "sales / total assets",
                      "equity / total assets",
                      "(gross profit + extraordinary items + financial expenses) / total assets",
                      "gross profit / short - term liabilities",
                      "(gross profit + depreciation) / sales",
                      "(gross profit + interest) / total assets",
                      "(total liabilities * 365) / (gross profit + depreciation)",
                      "(gross profit + depreciation) / total liabilities",
                      "total assets / total liabilities",
                      "gross profit / total assets",
                      "gross profit / sales",
                      "(inventory * 365) / sales",
                      "sales(n) / sales(n - 1)",
                      "profit on operating activities / total assets",
                      "net profit / sales",
                      "gross profit( in 3 years) / total assets",
                      "(equity - share capital) / total assets",
                      "(net profit + depreciation) / total liabilities",
                      "profit on operating activities / financial expenses",
                      "working capital / fixed assets",
                      "logarithm of total assets",
                      "(total liabilities - cash) / sales",
                      "(gross profit + interest) / sales",
                      "(current liabilities * 365) / cost of products sold",
                      "operating expenses / short - term liabilities",
                      "operating expenses / total liabilities",
                      "profit on sales / total assets",
                      "total sales / total assets",
                      "(current assets - inventories) / long - term liabilities",
                      "constant capital / total assets",
                      "profit on sales / sales",
                      "(current assets - inventory - receivables) / short - term liabilities",
                      "total liabilities / ((profit on operating activities + depreciation) * (12 / 365))",
                      "profit on operating activities / sales",
                      "rotation receivables + inventory turnover in days",
                      "(receivables * 365) / sales",
                      "net profit / inventory",
                      "(current assets - inventory) / short - term liabilities",
                      "(inventory * 365) / cost of products sold",
                      "EBITDA(profit on operating activities - depreciation) / total assets",
                      "EBITDA(profit on operating activities - depreciation) / sales",
                      "current assets / total liabilities",
                      "short - term liabilities / total assets",
                      "(short - term liabilities * 365) / cost of products sold)",
                      "equity / fixed assets",
                      "constant capital / fixed assets",
                      "working capital",
                      "(sales - cost of products sold) / sales",
                      "(current assets - inventory - short - term liabilities) / (sales - gross profit - depreciation)",
                      "total costs / total sales",
                      "long - term liabilities / equity",
                      "sales / inventory",
                      "sales / receivables",
                      "(short - term liabilities * 365) / sales",
                      "sales / short - term liabilities",
                      "sales / fixed assets",
                      "bankruptcy"]

    ds = ds.set_axis(variable_names, axis=1, inplace=False)

    # binary cat to int
    ds['bankruptcy'] = ds['bankruptcy'].map({b'0': 0, b'1': 1})

    # Removing variables with too many NaNs
    ds = ds.drop(['(current assets - inventories) / long - term liabilities',
                 'profit on operating activities / financial expenses', 'sales / inventory', 'net profit / inventory'], axis=1)

    # Dropping the remaining samples with NaNs
    ds = ds.dropna()

    # Dropping duplicatel lines
    ds = ds.drop_duplicates()

    y = ds['bankruptcy']
    X = ds.drop(['bankruptcy'], axis=1)

    if scale:
        scaler_1 = PowerTransformer()
        # scaler_2 = RobustScaler()
        X = scaler_1.fit_transform(X)
        # X = scaler_2.fit_transform(X)

    if splitXY:
        return X, y
    else:
        return ds


def load_preprocessed_congressional(splitXY=True):
    df = pd.read_csv(
        'data/kaggle_congressional_voting/CongressionalVotingID.shuf.lrn.csv')

    df = df.replace(['y', 'n', 'unknown', 'republican',
                     'democrat'], [0, 1, np.nan, 1, 0])

    X = df.drop(columns=['ID', 'class'])
    y = df['class']

    imputer = KNNImputer(n_neighbors=1)
    imputer.fit(X)
    X = imputer.transform(X)

    if splitXY:
        return X, y
    else:
        return df


def load_preprocessed_location(splitXY=True):
    df = pd.read_csv(
        "data/kaggle_location_dataset/Location446-30cls-5k.lrn.csv")
    geosocial_type = CategoricalDtype(
        categories=[f"{i}" for i in range(1, 31)], ordered=True)
    df['class'] = df['class'].apply(str).astype(geosocial_type)
    df = df.drop("ID", axis=1)
    df = df.sample(frac=1)

    X = df.iloc[:, 1:]
    y = df["class"]

    if splitXY:
        return X, y
    else:
        return df
